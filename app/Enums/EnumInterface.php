<?php
/**
 *
 * @ClassName EnumInterface
 * @Version 1.0
 * @Description
 */


namespace App\Enums;


interface EnumInterface
{
    public static function getDescription(string $value);
}
