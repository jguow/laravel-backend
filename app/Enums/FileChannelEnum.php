<?php
/**
 *
 * @ClassName FileChannelEnum
 * @Version 1.0
 * @Description
 */


namespace App\Enums;


use MyCLabs\Enum\Enum;

class FileChannelEnum extends Enum implements EnumInterface
{
    public const ADMIN = 1;

    public const CLIENT = 2;

    public static function getDescription(string $value)
    {
        $desc = [
            self::ADMIN => '后台端',
            self::CLIENT => '用户端',
        ];

        return $desc[$value] ?? $value;
    }
}
