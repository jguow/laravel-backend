<?php
/**
 *
 * @ClassName FileTypeEnum
 * @Version 1.0
 * @Description
 */


namespace App\Enums;


use MyCLabs\Enum\Enum;

class FileTypeEnum extends Enum implements EnumInterface
{
    // 图片
    public const IMAGE = 1;

    // 附件
    public const FILE = 2;

    // 视频
    public const VIDEO = 3;

    public static function getDescription(string $value)
    {
        $desc = [
            self::IMAGE => '图片',
            self::FILE => '附件',
            self::VIDEO => '视频',
        ];

        return $desc[$value] ?? $value;
    }
}
