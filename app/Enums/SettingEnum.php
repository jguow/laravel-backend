<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

final class SettingEnum extends Enum implements EnumInterface
{
    public const STORAGE =  'storage';

    public static function getDescription($value): string
    {
        $desc = [
            self::STORAGE => '上传配置',
        ];

        return $desc[$value] ?? $value;
    }


}
