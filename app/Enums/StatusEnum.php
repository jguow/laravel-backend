<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;


final class StatusEnum extends Enum implements EnumInterface
{
    // 启用
    public const ENABLE =   1;

    // 禁用
    public const DISABLE =   0;

    public static function getDescription(string $value)
    {
        $desc = [
            self::ENABLE => '可用',
            self::DISABLE => '禁用',
        ];

        return $desc[$value] ?? '';
    }
}
