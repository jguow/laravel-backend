<?php
/**
 *
 * @ClassName StorageEnum
 * @Version 1.0
 * @Description
 */


namespace App\Enums;


use MyCLabs\Enum\Enum;

final class StorageEnum extends Enum implements EnumInterface
{
    // 本地
    public const LOCAL = 'local';

    // 七牛云
    public const QINIU = 'qiniu';

    // 阿里云
    public const ALIYUN = 'oss';

    // 腾讯云
    public const QCLOUD = 'qcloud';

    public static function getDescription(string $value)
    {
        $desc = [
            self::LOCAL => '本地',
            self::QINIU => '七牛云',
            self::ALIYUN => '阿里云',
            self::QCLOUD => '腾讯云',
        ];

        return $desc[$value] ?? $value;
    }

    public static function getDisk(string $value)
    {
        $disk = [
            self::LOCAL => 'public',
            self::QINIU => 'qiniu',
            self::ALIYUN => 'oss',
            self::QCLOUD => 'qcloud',
        ];

        return $disk[$value] ?? 'public';
    }
}
