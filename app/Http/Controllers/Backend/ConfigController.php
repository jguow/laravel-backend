<?php


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Services\Base\ConfigService;
use Illuminate\Http\Request;

/**
 * Class ConfigController
 * @package App\Http\Controllers\Backend
 * @signName 配置项管理
 * @sign config
 */
class ConfigController extends Controller
{
    protected $configService;

    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
     * @Description 获取配置项
     * @param string $config 配置key
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $config)
    {
        $values = $this->configService->getItem($config);
        return $this->successData(compact('values'));
    }

    /**
     * @Description 更新配置项
     * @param string $config
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(string $config, Request $request)
    {
        $this->configService->saveItem($config, $request->post());
        return $this->success();
    }

    /**
     * @Description 删除配置缓存
     * @param string $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $key)
    {
        $this->configService->deleteCache($key);
        return $this->success();
    }
}
