<?php
/**
 *
 * @ClassName PassportController
 * @Version 1.0
 * @Description
 */


namespace App\Http\Controllers\Backend;


use App\Constants\BackendConstant;
use App\Constants\SystemConstant;
use App\Exceptions\ServiceException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AdminLoginPost;
use App\Models\Admin;
use App\Services\Base\CommonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class PassportController extends Controller
{
    public function login(AdminLoginPost $adminLoginPost)
    {
        ['username' => $username, 'password' => $password] = $adminLoginPost->fillData();
        $admin = Admin::whereUsername($username)->firstOrFail();
        if (!$admin->isValid()) {
            throw new ServiceException(SystemConstant::BAN_LOGIN);
        }
        if (!Hash::check($password, $admin->password)) {
            throw new ServiceException(SystemConstant::ERROR_ACCOUNT);
        }

        // jwt登录
        $now = Carbon::now();
        $token = auth(BackendConstant::AUTH_GUARD)->claims([
            'time' => $now->timestamp,
            'endpoint' => CommonService::getRequestEndpoint(),
        ])->login($admin);

        $admin->fill([
            'last_login_ip' => Request::getClientIp(),
            'last_login_at' => $now,
        ])->save();

        return $this->success(SystemConstant::LOGIN_SUCCESS, compact('token'));
    }

    public function logout()
    {
        Auth::logout();
        return $this->success(SystemConstant::LOGOUT_SUCCESS);
    }
}
