<?php
/**
 *
 * @ClassName UploadController
 * @Version 1.0
 * @Description
 */


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Upload\UploadFileListRequest;
use App\Http\Requests\Backend\Upload\UploadFileUpdateRequest;
use App\Models\UploadFile;
use App\Services\Base\CommonService;
use App\Services\Base\UploadService;
use Illuminate\Http\Request;

class UploadFileController extends Controller
{
    public function index(UploadFileListRequest $request)
    {
        [
            'page_size' => $page_size,
            'file_type' => $file_type,
            'file_name' => $file_name,
            'group_id' => $group_id,
            'storage' => $storage,
            'channel' => $channel,
        ] = $request->fillData();

        $data = UploadFile::query()
            ->when($file_name, function ($query, $keyword) {
                return $query
                    ->where('file_name', 'like', "%$keyword%");
            })
            ->when($channel > 0, function ($query, $channel) {
                return $query->where('channel', $channel);
            })
            ->when($group_id >= 0, function ($query, $group_id) {
                return $query->where('group_id', $group_id);
            })
            ->when($storage, function ($query, $storage) {
                return $query->where('storage', $storage);
            })
            ->orderByDesc('id')->paginate($page_size)->toArray();
        $list = CommonService::changePageDataFormat($data);
        return $this->successData(compact('list'));
    }

    public function store(Request $request, UploadService $service)
    {
        $result = $service->uploadFile($request);
        return $this->successData($result);
    }

    public function show()
    {
        return $this->success('开发中');
    }

    public function update(UploadFile $file, UploadFileUpdateRequest $request)
    {
        $params = $request->fillData();
        $file->fill($params)->saveOrFail();
        return $this->success();
    }

    public function destroy(UploadFile $file)
    {
        $file->delete();
        return $this->success();
    }
}
