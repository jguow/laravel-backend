<?php

declare(strict_types=1);

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Upload\UploadGroupPostRequest;
use App\Models\UploadGroup;

/**
 *
 * @ClassName UploadGroupController
 * @Version 1.0
 * @Description 文件分组
 * @package App\Http\Controllers\Backend
 */
class UploadGroupController extends Controller
{
    public function index()
    {
        $list = UploadGroup::query()
            ->orderByDesc('sort')
            ->get()
            ->toArray();
        $list && $list = get_data_tree($list);
        return $this->successData(compact('list'));
    }

    public function store(UploadGroupPostRequest $request)
    {
        $params = $request->fillData();
        UploadGroup::create($params);
        return $this->success();
    }

    public function show()
    {
        return $this->success('开发中');
    }

    public function update(UploadGroup $file_group, UploadGroupPostRequest $request)
    {
        $params = $request->fillData();
        $file_group->fill($params)->saveOrFail();
        return $this->success();
    }

    public function destroy(UploadGroup $file_group)
    {
        $file_group->delete();
        return $this->success();
    }
}
