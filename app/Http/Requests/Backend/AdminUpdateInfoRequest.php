<?php


namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

/**
 * 修改信息请求
 * Class AdminUpdatePwdRequest
 * @package App\Http\Requests\Backend
 */
class AdminUpdateInfoRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'nickname' => [
                'required',
                'max:30',
            ],
            'old_password' => [
                'alpha_num',
                'chinese',
                'between:6,18',
            ],
            'password' => [
                'confirmed',
                'alpha_num',
                'chinese',
                'between:6,18',
            ],
            'password_confirmation' => [
                'alpha_num',
                'chinese',
                'between:6,18',
            ],
        ];
    }

    public function fillData()
    {
        return [
            'nickname' => $this->post('nickname'),
            'old_password' => $this->post('old_password'),
            'password' => $this->post('password'),
            'password_confirmation' => $this->post('password_confirmation'),
        ];
    }
}
