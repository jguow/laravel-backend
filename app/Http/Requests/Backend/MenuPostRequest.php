<?php


namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

class MenuPostRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'pid'=>[
                'required',
                'integer',
                'between:0,10000',
            ],
            'title'=>[
                'required',
                'string',
                'max:20',
            ],
            'name'=>[
                'required',
                'string',
                'max:30',
            ],
            'icon'=>[
                'sometimes',
                'string',
                'max:30',
                'nullable'
            ],
            'status' => [
                'required',
                'integer',
                'in:0,1',
            ],
            'sort' => [
                'sometimes',
                'integer',
                'min:0',
            ],
        ];
    }

    public function fillData()
    {
        // TODO: Implement fillData() method.
    }
}
