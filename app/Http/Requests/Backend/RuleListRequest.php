<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\PagePost;

class RuleListRequest extends PagePost
{
    public function rules()
    {
        return array_merge(parent::rules(),[
            'pid'=>[
                'sometimes',
                'integer',
                'between:0,1000',
                'nullable',
            ],
            'name'=>[
                'sometimes',
                'string',
                'between:2,30',
                'nullable',
            ],
            'value'=>[
                'sometimes',
                'string',
                'between:3,50',
                'nullable',
            ],
        ]);
    }

    public function fillData()
    {
        return array_merge(parent::fillData(),[
            'pid'=> $this->get('pid'),
            'name'=> $this->get('name'),
            'value'=> $this->get('value'),
        ]);
    }
}
