<?php
/**
 *
 * @ClassName UploadFileListRequest
 * @Version 1.0
 * @Description
 */


namespace App\Http\Requests\Backend\Upload;


use App\Http\Requests\PagePost;

class UploadFileListRequest extends PagePost
{
    public function rules()
    {
        return array_merge(parent::rules(),[
            'fileName'=>[
                'sometimes',
                'string',
                'max:30',
                'nullable',
            ],
            'groupId'=>[
                'sometimes',
                'integer',
                'gte:-1',
                'nullable',
            ],
            'fileType'=>[
                'sometimes',
                'integer',
                'gte:-1',
                'nullable',
            ],
            'storage'=>[
                'sometimes',
                'string',
                'max:30',
                'nullable',
            ],
            'channel'=>[
                'sometimes',
                'integer',
                'gte:-1',
                'nullable',
            ],
        ]);
    }

    public function fillData()
    {
        return array_merge(parent::fillData(), [
            'file_type' => $this->get('fileType'),
            'file_name' => $this->get('fileName'),
            'group_id' => $this->get('groupId'),
            'storage' => $this->get('storage'),
            'channel' => $this->get('channel'),
        ]);
    }
}
