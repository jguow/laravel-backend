<?php
/**
 *
 * @ClassName UploadFileUpdateRequest
 * @Version 1.0
 * @Description
 */


namespace App\Http\Requests\Backend\Upload;


use App\Http\Requests\BaseRequest;

class UploadFileUpdateRequest extends BaseRequest
{


    public function fillData()
    {
        return [];
    }
}
