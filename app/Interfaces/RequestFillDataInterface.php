<?php
/**
 * my_laravel_8
 * ==================================================================
 * CopyRight © 2021-2099 沙屿沫
 * ==================================================================
 *
 * @ClassName RequestFillDataInterface
 * @Author shayvmo
 * @Date 2021-04-21 22:48 星期三
 * @Version 1.0
 * @Description
 */

namespace App\Interfaces;


interface RequestFillDataInterface
{
    public function fillData();
}
