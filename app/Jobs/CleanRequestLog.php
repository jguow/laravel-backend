<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CleanRequestLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $end_time;

    protected $table = 'request_logs';

    public function __construct($before_days = 30)
    {
        $this->end_time = now()->subDays($before_days);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('清理日志: '.$this->end_time);
        \DB::table($this->table)->where('created_at','<',$this->end_time)->delete();
    }
}
