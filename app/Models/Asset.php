<?php

namespace App\Models;


/**
 * @property int $id
 * @property string $storage_type  存储方式： public 服务器本地 qiniu 七牛云 oss 阿里云
 * @property string $name  文件名称
 * @property string $path  存储路径
 * @property string $md5_file  文件md5
 * @property int $size  文件大小，单位： B
 * @property string $mime_type  文件mime类型
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 */
class Asset extends BaseModel
{
    protected $table = 'assets';
    /**
     * @var array
     */
    protected $fillable = [
        'storage_type',
        'name',
        'path',
        'md5_file',
        'size',
        'mime_type',
    ];
}
