<?php
/**
 *
 * @ClassName UploadFile
 * @Version 1.0
 * @Description
 */


namespace App\Models;


class UploadFile extends BaseModel
{
    protected $table = 'upload_files';

    /**
     * @var array
     */
    protected $fillable = [
        'group_id',
        'channel',
        'storage',
        'domain',
        'file_type',
        'file_name',
        'file_path',
        'file_ext',
        'file_md5',
        'cover',
        'file_size',
        'uploader_id',
        'is_recycle',
    ];

    protected $appends = [
        'preview_url'
    ];

    public function getPreviewUrlAttribute()
    {
        return $this->attributes['domain'] . $this->attributes['file_path'];
    }
}
