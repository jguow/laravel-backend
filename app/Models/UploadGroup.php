<?php


namespace App\Models;

/**
 *
 * @ClassName UploadGroup
 * @Version 1.0
 * @Description
 * @package App\Models
 *
 * @property int $id
 * @property string $title  名称
 * @property int $pid  父级ID
 * @property int $sort  倒序
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 *
 */
class UploadGroup extends BaseModel
{
    protected $table = 'upload_groups';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'pid',
        'sort',
    ];
}
