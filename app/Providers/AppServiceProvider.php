<?php

namespace App\Providers;

use App\Http\Validators\ChineseTextValidator;
use App\Http\Validators\HashValidator;
use App\Http\Validators\IdNumberValidator;
use App\Http\Validators\PhoneValidator;
use App\Http\Validators\UsernameValidator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    protected $validators = [
        'phone' => PhoneValidator::class,
        'id_no' => IdNumberValidator::class,
        'hash' => HashValidator::class,
        'username' => UsernameValidator::class,
        'chinese' => ChineseTextValidator::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Gate::before(function ($user, $ability) {
            if ($user->isSuper()) {
                return true;
            }
        });

        // 注册验证器
        $this->registerValidators();
        // 打印sql log
        $this->dumpSqlLog();

        if (!$this->app->runningInConsole()) {
            // 自定义配置
//            $this->app->make(ConfigServiceInterface::class)->sync();
        }
    }

    /**
     * Register validators.
     */
    protected function registerValidators()
    {
        foreach ($this->validators as $rule => $validator) {
            Validator::extend($rule, "{$validator}@validate");
        }
    }

    /**
     * sql log
     */
    public function dumpSqlLog()
    {
        DB::listen(
            function ($sql) {
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $sql->bindings[$i] = "'$binding'";
                        }
                    }
                }

                // Insert bindings into query
                $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);

                $query = vsprintf($query, $sql->bindings);

                // SQL语句
                $path = 'logs' . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR . date('Y-m-d') . DIRECTORY_SEPARATOR;
                if (!is_dir(storage_path($path)) && !mkdir($concurrentDirectory = storage_path($path), 0777, true) && !is_dir($concurrentDirectory)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                }

                // 慢SQL
                $slowLogPath = 'logs' . DIRECTORY_SEPARATOR . 'slowSql' . DIRECTORY_SEPARATOR . date('Y-m-d') . DIRECTORY_SEPARATOR;
                if (!is_dir(storage_path($slowLogPath)) && !mkdir($concurrentDirectory = storage_path($slowLogPath), 777, true) && !is_dir($concurrentDirectory)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                }

                // Save the query to file
                $logFile = fopen(
                    storage_path($path . date('H') . '-query.log'),
                    'a+'
                );
                fwrite($logFile, '[' . date('Y-m-d H:i:s') . '] ' . $query . PHP_EOL . PHP_EOL);
                fclose($logFile);

                // 执行大于1s
                if ($sql->time > 1000) {
                    $logFile = fopen(
                        storage_path($slowLogPath . date('H') . '-query-slow.log'),
                        'a+'
                    );
                    fwrite($logFile, '[' . date('Y-m-d H:i:s') . '] ' . $query . PHP_EOL . PHP_EOL);
                    fclose($logFile);
                }
            }
        );
    }
}
