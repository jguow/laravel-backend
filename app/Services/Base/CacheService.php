<?php
/**
 *
 * @ClassName CacheService
 * @Version 1.0
 * @Description
 */


namespace App\Services\Base;


use App\Constants\SystemConstant;
use App\Enums\SettingEnum;
use Illuminate\Support\Facades\Cache;

class CacheService
{
    public function items()
    {
        return $this->getItems();
    }

    public function clear(array $keys)
    {
        foreach ($keys as $key) {
            Cache::has($key) && Cache::forget($key);
        }
    }

    private function getItems(): array
    {
        return [
            SettingEnum::STORAGE => [
                'key' => sprintf(SystemConstant::CONFIG_CACHE_KEY_STRING, SettingEnum::STORAGE),
                'name' => SettingEnum::getDescription(SettingEnum::STORAGE)
            ],
        ];
    }
}
