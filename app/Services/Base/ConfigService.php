<?php


namespace App\Services\Base;

use App\Constants\SystemConstant;
use App\Enums\SettingEnum;
use App\Enums\StorageEnum;
use App\Models\Config;
use Illuminate\Support\Facades\Cache;

class ConfigService
{
    public function getItem(string $key)
    {
        return Cache::remember($this->getConfigCacheKey($key), now()->addDay(), function () use ($key) {
            $default = $this->getDefaultData()[$key] ?? [];
            return Config::firstOrCreate(
                compact('key'),
                [
                    'desc' => $default['desc'] ?? '',
                    'values' => $default['values'] ?? []
                ]
            )->toArray()['values'];
        });
    }

    public function saveItem(string $key, $value)
    {
        $default = $this->getDefaultData()[$key] ?? [];
        $config = Config::updateOrCreate(
            compact('key'),
            [
                'desc' => $default['desc'] ?? '',
                'values' => $value
            ]
        );
        $this->deleteCache($key);
        return $config;
    }

    public function deleteCache(string $key): void
    {
        Cache::forget($this->getConfigCacheKey($key));
    }

    private function getDefaultData(): array
    {
        return [
            SettingEnum::STORAGE => [
                'key' => SettingEnum::STORAGE,
                'desc' => '上传设置',
                'values' => [
                    'default' => StorageEnum::LOCAL,
                    'engine' => [
                        StorageEnum::LOCAL => null,
                        StorageEnum::QINIU => [
                            'bucket' => '',
                            'access_key' => '',
                            'secret_key' => '',
                            'domain' => 'http://'
                        ],
                        StorageEnum::ALIYUN => [
                            'bucket' => '',
                            'access_key_id' => '',
                            'access_key_secret' => '',
                            'domain' => 'http://'
                        ],
                        StorageEnum::QCLOUD => [
                            'bucket' => '',
                            'region' => '',
                            'secret_id' => '',
                            'secret_key' => '',
                            'domain' => 'http://'
                        ],
                    ]
                ],
            ],
        ];
    }


    private function getConfigCacheKey(string $key): string
    {
        return sprintf(SystemConstant::CONFIG_CACHE_KEY_STRING, $key);
    }
}
