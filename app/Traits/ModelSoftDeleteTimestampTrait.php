<?php

namespace App\Traits;


use Illuminate\Support\Carbon;

trait ModelSoftDeleteTimestampTrait
{
    public function getDeletedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }
}
