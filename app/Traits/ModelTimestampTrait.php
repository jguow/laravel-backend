<?php

namespace App\Traits;


use Illuminate\Support\Carbon;

trait ModelTimestampTrait
{
    public function getCreatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }
}
