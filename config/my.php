<?php
/**
 * Author: shayvmo
 * CreateTime: 2020/6/1 14:12
 * Description: 自定义配置
 */

return [

    'success_code' => 'success',       // 成功

    'fail_code' => 'failed',        // 失败

    'result_code' => [                // 业务处理结果

        'success' => 200,             // 成功

        'fail' => 500,               // 失败

        'login_expire' => 401,          // 未登录或登录已失效

        'forbidden' => 403,             // 无权限

    ],

    // 系统
    'system' => [

        'system_open_tag' => 1, // 系统开放标识： 1 开放 0 未开放

        'login_only_one_place' => 0,// 登录互挤

    ],

    // 文件上传
    'upload' => [
        /*
         * 默认文件或文件组名称
         */
        'file_index' => 'file',

        /*
         * 默认文件最大10M，单位：MB
         */
        'file_size' => 10,

        /*
         * 允许上传的文件类型
         */
        'file_ext' => [
            'jpg',
            'jpeg',
            'png',
            'gif',
            'mp4',
        ],

        /*
         * 允许上传的文件mime类型
         */
        'file_mime' => [
            'image/jpeg',
            'image/png',
            'image/gif',
        ],
    ],
];
