<?php
/**
 * shayvmo-admin
 *
 * @Author Eric
 * @Date 2021-08-01 16:49 星期日
 * @Version 1.0
 * @Description 系统权限文件
 */

return [
//    [
//        'name' => 'index',
//        'sign' => '',
//        'title' => '首页',
//        'type' => \App\Enums\PermissionTypeEnum::MENU,
//        'sort' => 100,
//        'route' => '/index',
//        'method' => '',
//        'path' => '',
//        'children' => [],
//    ],
//    [
//        'name' => 'manage',
//        'sign' => '',
//        'title' => '管理员',
//        'type' => \App\Enums\PermissionTypeEnum::MENU,
//        'sort' => 100,
//        'route' => '/manage',
//        'method' => '',
//        'path' => '',
//        'children' => [
//            [
//                'name' => 'manage-admin',
//                'sign' => '',
//                'title' => '管理员列表',
//                'type' => \App\Enums\PermissionTypeEnum::MENU,
//                'sort' => 100,
//                'route' => '/manage/user/index',
//                'method' => '',
//                'path' => '',
//                'children' => [
//                    [
//                        'name' => 'manage-admin-add',
//                        'sign' => 'add',
//                        'title' => '添加',
//                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
//                        'sort' => 100,
//                        'route' => '',
//                        'method' => '',
//                        'path' => '',
//                    ],
//                    [
//                        'name' => 'manage-admin-edit',
//                        'sign' => 'edit',
//                        'title' => '编辑',
//                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
//                        'sort' => 100,
//                        'route' => '',
//                        'method' => '',
//                        'path' => '',
//                    ],
//                    [
//                        'name' => 'manage-admin-delete',
//                        'sign' => 'delete',
//                        'title' => '删除',
//                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
//                        'sort' => 100,
//                        'route' => '',
//                        'method' => '',
//                        'path' => '',
//                    ],
//                ],
//            ],
//            [
//                'name' => 'manage-role',
//                'sign' => '',
//                'title' => '角色管理',
//                'type' => \App\Enums\PermissionTypeEnum::MENU,
//                'sort' => 100,
//                'route' => '/manage/role/index',
//                'method' => '',
//                'path' => '',
//                'children' => [
//                    [
//                        'name' => 'manage-role-add',
//                        'sign' => 'add',
//                        'title' => '添加',
//                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
//                        'sort' => 100,
//                        'route' => '',
//                        'method' => '',
//                        'path' => '',
//                    ],
//                    [
//                        'name' => 'manage-role-edit',
//                        'sign' => 'edit',
//                        'title' => '编辑',
//                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
//                        'sort' => 100,
//                        'route' => '',
//                        'method' => '',
//                        'path' => '',
//                    ],
//                    [
//                        'name' => 'manage-role-delete',
//                        'sign' => 'delete',
//                        'title' => '删除',
//                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
//                        'sort' => 100,
//                        'route' => '',
//                        'method' => '',
//                        'path' => '',
//                    ],
//                ],
//            ],
//            [
//                'name' => 'manage-log',
//                'sign' => '',
//                'title' => '操作日志',
//                'type' => \App\Enums\PermissionTypeEnum::MENU,
//                'sort' => 100,
//                'route' => '/manage/log/index',
//                'method' => '',
//                'path' => '',
//                'children' => [],
//            ],
//        ],
//    ],








    [
        'name' => 'system',
        'sign' => 'system',
        'title' => '系统权限',
        'type' => \App\Enums\PermissionTypeEnum::MENU,
        'sort' => 100,
        'route' => '',
        'method' => '',
        'path' => '',
        'children' => [
            [
                'name' => 'system-admin',
                'sign' => 'admin',
                'title' => '管理员列表',
                'type' => \App\Enums\PermissionTypeEnum::MENU,
                'sort' => 100,
                'route' => '',
                'method' => '',
                'path' => '',
                'children' => [
                    [
                        'name' => 'add-system-admin',
                        'sign' => '',
                        'title' => '添加管理员按钮',
                        'type' => \App\Enums\PermissionTypeEnum::BUTTON,
                        'sort' => 0,
                        'route' => '',
                        'method' => '',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/admin-GET',// 路由
                        'sign' => '',
                        'title' => '获取管理员列表/信息',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/admin',
                        'method' => 'GET',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/admin-POST',// 路由
                        'sign' => '',
                        'title' => '新增管理员',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/admin',
                        'method' => 'POST',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/admin-PUT',// 路由
                        'sign' => '',
                        'title' => '编辑管理员',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/admin',
                        'method' => 'PUT',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/admin-DELETE',// 路由
                        'sign' => '',
                        'title' => '删除管理员',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/admin',
                        'method' => 'DELETE',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/admin/resetPassword-PUT',// 路由
                        'sign' => '',
                        'title' => '重置密码',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/admin/resetPassword',
                        'method' => 'PUT',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/admin/forbidden-PUT',// 路由
                        'sign' => '',
                        'title' => '禁用管理员',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/admin/forbidden',
                        'method' => 'PUT',
                        'path' => '',
                    ],
                ],
            ],
            [
                'name' => 'system-role',
                'sign' => 'role',
                'title' => '角色列表',
                'type' => \App\Enums\PermissionTypeEnum::MENU,
                'sort' => 100,
                'route' => '',
                'method' => '',
                'path' => '',
                'children' => [
                    [
                        'name' => '/backend/role-GET',// 路由
                        'sign' => '',
                        'title' => '角色列表',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/role',
                        'method' => 'GET',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/role-POST',// 路由
                        'sign' => '',
                        'title' => '新增角色',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/role',
                        'method' => 'POST',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/role-PUT',// 路由
                        'sign' => '',
                        'title' => '编辑角色',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/role',
                        'method' => 'PUT',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/role-DELETE',// 路由
                        'sign' => '',
                        'title' => '删除角色',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/role',
                        'method' => 'DELETE',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/role/permission-GET',// 路由
                        'sign' => '',
                        'title' => '获取角色权限',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/role/permission',
                        'method' => 'GET',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/role/permission-PUT',// 路由
                        'sign' => '',
                        'title' => '设置角色权限',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/role/permission',
                        'method' => 'PUT',
                        'path' => '',
                    ],
                ],
            ],
            [
                'name' => 'system-permission',
                'sign' => 'permission',
                'title' => '权限列表',
                'type' => \App\Enums\PermissionTypeEnum::MENU,
                'sort' => 100,
                'route' => '',
                'method' => '',
                'path' => '',
                'children' => [
                    [
                        'name' => '/backend/permission-GET',// 路由
                        'sign' => '',
                        'title' => '权限列表',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/permission',
                        'method' => 'GET',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/permission-POST',// 路由
                        'sign' => '',
                        'title' => '新增权限',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/permission',
                        'method' => 'POST',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/permission-PUT',// 路由
                        'sign' => '',
                        'title' => '保存权限',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/permission',
                        'method' => 'PUT',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/permission-DELETE',// 路由
                        'sign' => '',
                        'title' => '删除权限',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/permission',
                        'method' => 'DELETE',
                        'path' => '',
                    ],
                ],
            ],
            [
                'name' => 'system-config',
                'sign' => 'config',
                'title' => '配置项管理',
                'type' => \App\Enums\PermissionTypeEnum::MENU,
                'sort' => 100,
                'route' => '',
                'method' => '',
                'path' => '',
                'children' => [
                    [
                        'name' => '/backend/config-GET',// 路由
                        'sign' => '',
                        'title' => '配置项列表',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/config',
                        'method' => 'GET',
                        'path' => '',
                    ],
                    [
                        'name' => '/backend/config-POST',// 路由
                        'sign' => '',
                        'title' => '新增配置项',
                        'type' => \App\Enums\PermissionTypeEnum::API,
                        'sort' => 0,
                        'route' => '/backend/config',
                        'method' => 'POST',
                        'path' => '',
                    ],
                ],
            ],
        ],
    ],

];
